import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler

def load_data(file_path):
    """Load the dataset from a CSV file."""
    return pd.read_csv(file_path)

def convert_dates(df, date_columns):
    """Convert columns to datetime."""
    df[date_columns] = df[date_columns].apply(pd.to_datetime, format='%d-%m-%Y')
    return df

def handle_missing_values(df):
    """Handle missing values in the dataset."""
    df['Returns'] = df['Returns'].astype(str).fillna('No')
    df['ind1'] = df['ind1'].astype(str).fillna('Unknown')
    df['ind2'] = df['ind2'].astype(str).fillna('Unknown')
    return df

def encode_categorical(df):
    """Encode categorical variables."""
    df = pd.get_dummies(df, columns=['Ship Mode', 'Segment', 'Country', 'City', 'State', 'Region', 'Category', 'Sub-Category', 'Payment Mode'], drop_first=True)
    return df

def standardize_numerical(df, numerical_columns):
    """Standardize numerical features."""
    scaler = StandardScaler()
    df[numerical_columns] = scaler.fit_transform(df[numerical_columns])
    return df

def process_data(file_path, output_path):
    """Complete data processing pipeline."""
    df = load_data(file_path)
    
    date_columns = ['Order Date', 'Ship Date']
    df = convert_dates(df, date_columns)
    
    df = handle_missing_values(df)
    
    df = encode_categorical(df)
    
    numerical_columns = ['Sales', 'Quantity', 'Profit']
    df = standardize_numerical(df, numerical_columns)
    
    df.to_csv(output_path, index=False)
    print(f"Data processing completed and saved to {output_path}")

if __name__ == "__main__":
    input_file_path = '../data/raw/mall_sales_data.csv'
    output_file_path = '../data/processed/processed_mall_sales_data.csv'
    
    process_data(input_file_path, output_file_path)
