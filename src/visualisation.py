import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os

def load_processed_data(file_path):
    """Load the processed dataset from a CSV file."""
    return pd.read_csv(file_path)

def create_dir_if_not_exists(directory):
    """Create directory if it does not exist."""
    if not os.path.exists(directory):
        os.makedirs(directory)

def plot_sales_distribution(df, output_path):
    """Plot and save the sales distribution graph."""
    plt.figure(figsize=(10, 6))
    sns.histplot(df['Sales'], bins=30, kde=True)
    plt.title('Sales Distribution')
    plt.xlabel('Sales')
    plt.ylabel('Frequency')
    plt.savefig(os.path.join(output_path, 'sales_distribution.png'))
    plt.close()

def plot_profit_distribution(df, output_path):
    """Plot and save the profit distribution graph."""
    plt.figure(figsize=(10, 6))
    sns.histplot(df['Profit'], bins=30, kde=True, color='orange')
    plt.title('Profit Distribution')
    plt.xlabel('Profit')
    plt.ylabel('Frequency')
    plt.savefig(os.path.join(output_path, 'profit_distribution.png'))
    plt.close()

def plot_sales_vs_quantity(df, output_path):
    """Plot and save the sales vs. quantity scatter plot."""
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='Quantity', y='Sales', data=df)
    plt.title('Sales vs. Quantity')
    plt.xlabel('Quantity')
    plt.ylabel('Sales')
    plt.savefig(os.path.join(output_path, 'sales_vs_quantity.png'))
    plt.close()

def plot_profit_vs_quantity(df, output_path):
    """Plot and save the profit vs. quantity scatter plot."""
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='Quantity', y='Profit', data=df, color='green')
    plt.title('Profit vs. Quantity')
    plt.xlabel('Quantity')
    plt.ylabel('Profit')
    plt.savefig(os.path.join(output_path, 'profit_vs_quantity.png'))
    plt.close()

def plot_sales_by_category(df, output_path):
    """Plot and save the sales by category bar chart."""
    if 'Category' not in df.columns:
        print(f"Columns in DataFrame: {df.columns.tolist()}")
        raise KeyError("The 'Category' column is not found in the DataFrame.")
    plt.figure(figsize=(12, 8))
    df.groupby('Category')['Sales'].sum().plot(kind='bar')
    plt.title('Total Sales by Category')
    plt.xlabel('Category')
    plt.ylabel('Total Sales')
    plt.savefig(os.path.join(output_path, 'sales_by_category.png'))
    plt.close()

def plot_profit_by_category(df, output_path):
    """Plot and save the profit by category bar chart."""
    if 'Category' not in df.columns:
        print(f"Columns in DataFrame: {df.columns.tolist()}")
        raise KeyError("The 'Category' column is not found in the DataFrame.")
    plt.figure(figsize=(12, 8))
    df.groupby('Category')['Profit'].sum().plot(kind='bar', color='red')
    plt.title('Total Profit by Category')
    plt.xlabel('Category')
    plt.ylabel('Total Profit')
    plt.savefig(os.path.join(output_path, 'profit_by_category.png'))
    plt.close()

def visualize_data(input_file_path, output_folder):
    """Main function to visualize data and save plots."""
    create_dir_if_not_exists(output_folder)
    df = load_processed_data(input_file_path)
    
    print(f"Columns in loaded DataFrame: {df.columns.tolist()}")  # Debug print to inspect columns
    
    plot_sales_distribution(df, output_folder)
    plot_profit_distribution(df, output_folder)
    plot_sales_vs_quantity(df, output_folder)
    plot_profit_vs_quantity(df, output_folder)
    plot_sales_by_category(df, output_folder)
    plot_profit_by_category(df, output_folder)

if __name__ == "__main__":
    input_file_path = '../data/processed/processed_mall_sales_data.csv'
    output_folder = '../assets'
    
    visualize_data(input_file_path, output_folder)
