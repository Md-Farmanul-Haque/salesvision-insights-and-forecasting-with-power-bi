import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from xgboost import XGBRegressor

def load_data(file_path):
    """Load the processed dataset from a CSV file."""
    return pd.read_csv(file_path)

def create_dir_if_not_exists(directory):
    """Create directory if it does not exist."""
    if not os.path.exists(directory):
        os.makedirs(directory)

def train_sales_forecast_model(df):
    """Train multiple models and return the best one based on hyperparameter tuning."""
    # Prepare the features and target variable
    X = df[['Quantity', 'Profit', 'Order Date', 'Ship Date']].copy()
    y = df['Sales']
    
    # Convert dates to ordinal
    X['Order Date'] = pd.to_datetime(X['Order Date']).map(pd.Timestamp.toordinal)
    X['Ship Date'] = pd.to_datetime(X['Ship Date']).map(pd.Timestamp.toordinal)
    
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    
    # Define parameter grids for RandomizedSearchCV
    param_grid_rf = {
        'n_estimators': [100, 200, 300, 400, 500],
        'max_depth': [None, 10, 20, 30, 40, 50],
        'min_samples_split': [2, 5, 10],
        'min_samples_leaf': [1, 2, 4]
    }
    
    param_grid_gb = {
        'n_estimators': [100, 200, 300, 400, 500],
        'learning_rate': [0.01, 0.05, 0.1, 0.2],
        'max_depth': [3, 5, 7, 9],
        'subsample': [0.7, 0.8, 0.9, 1.0]
    }

    param_grid_xgb = {
        'n_estimators': [100, 200, 300, 400, 500],
        'learning_rate': [0.01, 0.05, 0.1, 0.2],
        'max_depth': [3, 5, 7, 9],
        'subsample': [0.7, 0.8, 0.9, 1.0],
        'colsample_bytree': [0.7, 0.8, 0.9, 1.0]
    }
    
    # Initialize models
    models = {
        'RandomForest': RandomizedSearchCV(RandomForestRegressor(random_state=42), param_grid_rf, cv=5, n_jobs=-1, scoring='neg_mean_squared_error', random_state=42),
        'GradientBoosting': RandomizedSearchCV(GradientBoostingRegressor(random_state=42), param_grid_gb, cv=5, n_jobs=-1, scoring='neg_mean_squared_error', random_state=42),
        'XGBoost': RandomizedSearchCV(XGBRegressor(random_state=42, objective='reg:squarederror'), param_grid_xgb, cv=5, n_jobs=-1, scoring='neg_mean_squared_error', random_state=42)
    }
    
    # Train models and evaluate performance
    best_model = None
    best_score = float('inf')
    for name, model in models.items():
        model.fit(X_train, y_train)
        score = model.best_score_
        print(f"{name} Best Score: {score}")
        if score < best_score:
            best_score = score
            best_model = model
    
    return best_model, X_test, y_test

def evaluate_model(model, X_test, y_test, output_path):
    """Evaluate the model and save evaluation metrics and plots."""
    y_pred = model.predict(X_test)
    
    mae = mean_absolute_error(y_test, y_pred)
    mse = mean_squared_error(y_test, y_pred)
    rmse = np.sqrt(mse)
    r2 = r2_score(y_test, y_pred)
    
    # Print metrics
    print(f'MAE: {mae}')
    print(f'MSE: {mse}')
    print(f'RMSE: {rmse}')
    print(f'R^2: {r2}')
    
    # Save metrics to a text file
    metrics_file = os.path.join(output_path, 'model_metrics.txt')
    with open(metrics_file, 'w') as f:
        f.write(f'MAE: {mae}\n')
        f.write(f'MSE: {mse}\n')
        f.write(f'RMSE: {rmse}\n')
        f.write(f'R^2: {r2}\n')
    
    # Plot and save the actual vs predicted sales
    plt.figure(figsize=(10, 6))
    plt.scatter(y_test, y_pred, alpha=0.5)
    plt.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()], 'k--', lw=2)
    plt.xlabel('Actual Sales')
    plt.ylabel('Predicted Sales')
    plt.title('Actual vs Predicted Sales')
    plt.savefig(os.path.join(output_path, 'actual_vs_predicted_sales.png'))
    plt.close()

def visualize_data(input_file_path, output_folder):
    """Main function to visualize data and save plots."""
    create_dir_if_not_exists(output_folder)
    df = load_data(input_file_path)
    
    model, X_test, y_test = train_sales_forecast_model(df)
    evaluate_model(model, X_test, y_test, output_folder)

if __name__ == "__main__":
    input_file_path = '../data/processed/processed_mall_sales_data.csv'
    output_folder = '../assets/model_evaluation'
    
    visualize_data(input_file_path, output_folder)
