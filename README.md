# SalesVision: Insights and Forecasting with Power BI

SalesVision is a comprehensive sales analysis and forecasting project that leverages the powerful data visualization and analytical capabilities of Power BI. This project aims to provide in-depth insights into sales performance, identify key trends, and forecast future sales to drive strategic decision-making. With SalesVision, businesses can visualize their sales data, track performance metrics, and predict future sales outcomes, all within an intuitive Power BI dashboard.


## Table of Contents

- [Introduction](#introduction)

- [Directory Structure](#directory-structure)

- [Prerequisites](#prerequisites)

- [Installation](#installation)

- [Usage](#usage)

- [Data](#data)

- [Features](#features)

- [License](#license)

- [Contact](#contact)

## Introduction

SalesVision helps businesses to gain a clear understanding of their sales data through comprehensive analysis and forecasting. By utilizing Power BI, users can interact with their sales data, uncover insights, and make data-driven decisions to improve sales strategies and outcomes.

## Directory Structure

```
SalesVision/
├── assets/
│   └── model_evaluation/
│       ├── actual_vs_predicted_sales.png
│       └── model_metrics.txt
│
├── data/
│   ├── raw/
│   │   └── mall_sales_data.csv
│   └── processed/ 
│       └── processed_mall_sales_data.csv
│
├── notebooks/
│   ├── data_preprocessing.ipynb
│   ├── exploratory_data_analysis.ipynb
│   └── model_evaluation.ipynb
│
├── reports/
│   └── sales_analysis_and_projection.pbix
│
├── src/
│   ├── data_preprocessing.py
│   ├── model_evaluation.py
│   └── visualization.py
│
├── LICENSE
├── README.md
└── requirements.txt
```

## Prerequisites

To run this project, you will need the following software and libraries:

- Python 3.x
- Power BI Desktop
- Jupyter Notebook
- Pandas
- NumPy
- Scikit-learn
- Matplotlib
- Seaborn

## Installation

1. **Clone the repository**:

```bash
git clone https://gitlab.com/Md-Farmanul-Haque/salesvision-insights-and-forecasting-with-power-bi.git
cd salesvision-insights-and-forecasting-with-power-bi
```

2. **Install the required libraries**:

You can install the required libraries using pip:

```bash
pip install -r requirements.txt
```

## Usage

### Data Preprocessing

Navigate to the `notebooks` directory and open the `data_preprocessing.ipynb` notebook. This notebook will guide you through the data cleaning and preprocessing steps.

### Exploratory Data Analysis

Open the `exploratory_data_analysis.ipynb` notebook to perform exploratory data analysis (EDA) on the sales data. This will help you understand the data and uncover key insights.

### Model Evaluation

Use the `model_evaluation.ipynb` notebook to evaluate different sales forecasting models. This notebook includes code for training and evaluating models, as well as selecting the best model for forecasting.

### Power BI Report

The `reports` directory contains the `SalesVision_Report.pbit` Power BI template file. Open this file in Power BI Desktop to interact with the sales dashboard, visualize key metrics, and explore forecasts.

## Data

The dataset used in this project is `mall_sales_data.csv`, which contains sales data from a retail mall. The columns in the dataset include:

- `Row ID`
- `Order ID`
- `Order Date`
- `Ship Date`
- `Ship Mode`
- `Customer ID`
- `Customer Name`
- `Segment`
- `Country`
- `City`
- `State`
- `Region`
- `Product ID`
- `Category`
- `Sub-Category`
- `Product Name`
- `Sales`
- `Quantity`
- `Profit`
- `Returns`
- `Payment Mode`
- `ind1`
- `ind2`

## Features

- **Sales Analysis**: Analyze sales data to understand trends, patterns, and key metrics.
- **Interactive Dashboards**: Visualize sales data through interactive Power BI dashboards.
- **Forecasting**: Predict future sales using machine learning models and evaluate their performance.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contact

For any questions or feedback, please contact:

- **Md Farmanul Haque**
  - [Email](mailto:farmanhaque74@gmail.com)
  - [GitLab Profile](https://gitlab.com/Md-Farmanul-Haque)

---